#include "KawaiiMisakaFactory.hpp"
#include "MisakaContext3D.hpp"
#include "MisakaRootImpl.hpp"

#include <Kawaii3D/KawaiiRoot.hpp>
#include <QWindow>

extern "C" MISAKA3D_SHARED_EXPORT KawaiiRendererImpl* createMisakaRoot(KawaiiRoot *root)
{
  return KawaiiMisakaFactory::getInstance()->createRenderer(*root);
}

extern "C" MISAKA3D_SHARED_EXPORT std::unique_ptr<QWindow> createMisakaWindow()
{
  auto wnd = std::make_unique<QWindow>();
  wnd->setSurfaceType(QSurface::OpenGLSurface);
  wnd->setFormat(MisakaContext3D::requestedFormat(QSurfaceFormat::defaultFormat()));
  return wnd;
}

extern "C" MISAKA3D_SHARED_EXPORT bool checkMisaka()
{
  return MisakaContext3D::checkSystem();
}
