#ifndef MISAKAGLBUFFER_HPP
#define MISAKAGLBUFFER_HPP

#include "MisakaGLObject.hpp"
#include "MisakaGLBufferType.hpp"
#include <QVector>
#include <type_traits>

class MISAKA3D_SHARED_EXPORT MisakaGLBuffer : public MisakaGLObject
{
  Q_OBJECT

  friend class MisakaBufferImpl;

public:
  template<typename... ArgsT>
  inline MisakaGLBuffer(MisakaGLBufferType type, ArgsT&&... args):
    MisakaGLObject(std::forward<ArgsT>(args)...),
    type(type),
    data(nullptr), dataSize(0),
    justInitialized(false)
  { }

  ~MisakaGLBuffer();

  template<typename T>
  inline const T* getData() const
  { Q_ASSERT(dataSize%sizeof(T) == 0); return reinterpret_cast<const T*>(data); }

  void setData(const void *ptr, size_t size);

  bool bind();
  bool bindToBlock(GLuint blockIndex, MisakaGLBufferType target) const;
  bool bindToBlock(GLuint blockIndex) const;
  bool bindVertexBuffer(GLuint vaobj, GLuint bindingindex, GLintptr offset, GLsizei stride) const;
  bool bindElementBuffer(GLuint vaobj) const;

  bool unbindBlock(GLuint blockIndex, MisakaGLBufferType target) const;

  void updateData(size_t offset, size_t n);

  void updateData(const void *subData, size_t n);

  size_t getDataSize() const;

  // MisakaGLObject interface  
protected:
  void initialize(GLuint &id) override;
  void updateNow() override;



  //IMPLEMENT
private:
  struct Interval {
    size_t i, n;
  };

  QLinkedList<Interval> dirtyIntervals;
  MisakaGLBufferType type;
  const void *data;
  size_t dataSize;
  bool justInitialized;

  bool mergeIntervals(const Interval &a, Interval &b);
  void condenseDirtyIntervals(const QLinkedList<Interval>::iterator &el);
};

template<>
inline const void* MisakaGLBuffer::getData<void>() const
{ return data; }

#endif // MISAKAGLBUFFER_HPP
