#include "MisakaFunctions.hpp"
#include "Textures/MisakaTextureType.hpp"

#include <algorithm>
#include <QSet>

MisakaFunctions::MisakaFunctions():
  isImageHandleResident (nullptr),
  isTextureHandleResident (nullptr),
  programUniformHandleui64v (nullptr),
  programUniformHandleui64 (nullptr),
  uniformHandleui64v (nullptr),
  uniformHandleui64 (nullptr),
  makeImageHandleNonResident (nullptr),
  makeImageHandleResident (nullptr),
  getImageHandle (nullptr),
  makeTextureHandleNonResident (nullptr),
  makeTextureHandleResident (nullptr),
  getTextureSamplerHandle (nullptr),
  getTextureHandle (nullptr),

  createTextures (nullptr),
  textureStorage1D (nullptr),
  textureStorage2D (nullptr),
  textureStorage3D (nullptr),

  createBuffers (nullptr),
  namedBufferStorage (nullptr),
  namedBufferSubData (nullptr),

  createVertexArrays (nullptr),
  vertexArrayVertexBuffer (nullptr),
  enableVertexArrayAttrib (nullptr),
  disableVertexArrayAttrib (nullptr),
  vertexArrayAttribFormat (nullptr),
  vertexArrayAttribBinding (nullptr),
  vertexArrayElementBuffer (nullptr),

  createFramebuffers (nullptr),
  namedFramebufferTexture (nullptr),

  createRenderbuffers (nullptr),
  namedRenderbufferStorage (nullptr),
  namedFramebufferRenderbuffer (nullptr),

  maxAnisotropy (1),
  nvShaders5 (false)
{
}

MisakaFunctions::~MisakaFunctions()
{
}

MisakaFunctions::DriverIssues MisakaFunctions::check() const
{
  DriverIssues result = {false, false, false, false, false, false};

  result.No_BindlessTexture = !(
        isImageHandleResident &&
        isTextureHandleResident &&
        programUniformHandleui64v &&
        programUniformHandleui64 &&
        uniformHandleui64v &&
        uniformHandleui64 &&
        makeImageHandleNonResident &&
        makeImageHandleResident &&
        getImageHandle &&
        makeTextureHandleNonResident &&
        makeTextureHandleResident &&
        getTextureSamplerHandle &&
        getTextureHandle);

  result.No_DSA_Textures = !(
        createTextures &&
        textureStorage1D &&
        textureStorage2D &&
        textureStorage3D);

  result.No_DSA_Buffers = !(
        createBuffers &&
        namedBufferStorage &&
        namedBufferSubData);

  result.No_DSA_VAO = !(
        createVertexArrays &&
        vertexArrayVertexBuffer &&
        enableVertexArrayAttrib &&
        disableVertexArrayAttrib &&
        vertexArrayAttribFormat &&
        vertexArrayAttribBinding &&
        vertexArrayElementBuffer);

  result.No_DSA_FBO = !(
        createFramebuffers &&
        namedFramebufferTexture);

  result.No_DSA_RBO = !(
        createRenderbuffers &&
        namedRenderbufferStorage &&
        namedFramebufferRenderbuffer);

  return result;
}

void MisakaFunctions::glShaderSource(GLuint shader, const QString &source)
{
  GLint length = source.length();
  auto str = reinterpret_cast<char*>(malloc(sizeof(char) * length));
  for(int i = 0; i < length; ++i)
    str[i] = source.at(i).cell();

  QOpenGLExtraFunctions::glShaderSource(shader, 1, const_cast<const char**>(&str), &length);
  free(str);
}

GLboolean MisakaFunctions::glIsImageHandleResident(GLuint64 handle)
{
  return (*isImageHandleResident)(handle);
}

GLboolean MisakaFunctions::glIsTextureHandleResident(GLuint64 handle)
{
  return (*isTextureHandleResident)(handle);
}

void MisakaFunctions::glProgramUniformHandleui64v(GLuint program, GLint location, GLsizei count, const GLuint64 *values)
{
  (*programUniformHandleui64v)(program, location, count, values);
}

void MisakaFunctions::glProgramUniformHandleui64(GLuint program, GLint location, GLuint64 value)
{
  (*programUniformHandleui64)(program, location, value);
}

void MisakaFunctions::glUniformHandleui64v(GLint location, GLsizei count, const GLuint64 *value)
{
  (*uniformHandleui64v)(location, count, value);
}

void MisakaFunctions::glUniformHandleui64(GLint location, GLuint64 value)
{
  (*uniformHandleui64)(location, value);
}

void MisakaFunctions::glMakeImageHandleNonResident(GLuint64 handle)
{
  (*makeImageHandleNonResident)(handle);
}

void MisakaFunctions::glMakeImageHandleResident(GLuint64 handle, GLenum access)
{
  return (*makeImageHandleResident)(handle, access);
}

GLuint64 MisakaFunctions::glGetImageHandle(GLuint texture, GLint level, GLboolean layered, GLint layer, GLenum format)
{
  return (*getImageHandle)(texture, level, layered, layer, format);
}

void MisakaFunctions::glMakeTextureHandleNonResident(GLuint64 handle)
{
  return (*makeTextureHandleNonResident)(handle);
}

void MisakaFunctions::glMakeTextureHandleResident(GLuint64 handle)
{
  return (*makeTextureHandleResident)(handle);
}

GLuint64 MisakaFunctions::glGetTextureSamplerHandle(GLuint texture, GLuint sampler)
{
  return (*getTextureSamplerHandle)(texture, sampler);
}

GLuint64 MisakaFunctions::glGetTextureHandle(GLuint texture)
{
  return (*getTextureHandle)(texture);
}

void MisakaFunctions::glCreateTextures(MisakaTextureType target, GLsizei n, GLuint *textures)
{
  (*createTextures)(static_cast<GLenum>(target), n, textures);
}

void MisakaFunctions::glTextureStorage1D(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width)
{
  (*textureStorage1D)(texture, levels, internalformat, width);
}

void MisakaFunctions::glTextureStorage2D(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height)
{
  (*textureStorage2D)(texture, levels, internalformat, width, height);
}

void MisakaFunctions::glTextureStorage3D(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth)
{
  (*textureStorage3D)(texture, levels, internalformat, width, height, depth);
}

void MisakaFunctions::glTextureSubImage1D(GLuint texture, GLint level, GLint xoffset, GLsizei width, GLenum format, MisakaGLVariableType type, const void *pixels)
{
  (*textureSubImage1D)(texture, level, xoffset, width, format, static_cast<GLenum>(type), pixels);
}

void MisakaFunctions::glTextureSubImage2D(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, MisakaGLVariableType type, const void *pixels)
{
  (*textureSubImage2D)(texture, level, xoffset, yoffset, width, height, format, static_cast<GLenum>(type), pixels);
}

void MisakaFunctions::glTextureSubImage3D(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, MisakaGLVariableType type, const void *pixels)
{
  (*textureSubImage3D)(texture, level, xoffset, yoffset, zoffset, width, height, depth, format, static_cast<GLenum>(type), pixels);
}

void MisakaFunctions::glCreateBuffers(GLsizei n, GLuint *buffers)
{
  (*createBuffers)(n, buffers);
}

void MisakaFunctions::glNamedBufferStorage(GLuint buffer, GLsizei size, const void *data, GLbitfield flags)
{
  (*namedBufferStorage)(buffer, size, data, flags);
}

void MisakaFunctions::glNamedBufferSubData(GLuint buffer, GLintptr offset, GLsizei size, const void *data)
{
  (*namedBufferSubData)(buffer, offset, size, data);
}

void MisakaFunctions::glCreateVertexArrays(GLsizei n, GLuint *arrays)
{
  (*createVertexArrays)(n, arrays);
}

void MisakaFunctions::glVertexArrayVertexBuffer(GLuint vaobj, GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride)
{
  (*vertexArrayVertexBuffer)(vaobj, bindingindex, buffer, offset, stride);
}

void MisakaFunctions::glEnableVertexArrayAttrib(GLuint vaobj, GLuint index)
{
  (*enableVertexArrayAttrib)(vaobj, index);
}

void MisakaFunctions::glDisableVertexArrayAttrib(GLuint vaobj, GLuint index)
{
  (*disableVertexArrayAttrib)(vaobj, index);
}

void MisakaFunctions::glVertexArrayAttribFormat(GLuint vaobj, GLuint attribindex, GLint size, MisakaGLVariableType type, GLboolean normalized, GLuint relativeoffset)
{
  (*vertexArrayAttribFormat)(vaobj, attribindex, size, static_cast<GLenum>(type), normalized, relativeoffset);
}

void MisakaFunctions::glVertexArrayAttribBinding(GLuint vaobj, GLuint attribindex, GLuint bindingindex)
{
  (*vertexArrayAttribBinding)(vaobj, attribindex, bindingindex);
}

void MisakaFunctions::glVertexArrayElementBuffer(GLuint vaobj, GLuint buffer)
{
  (*vertexArrayElementBuffer)(vaobj, buffer);
}

void MisakaFunctions::glCreateFramebuffers(GLsizei n, GLuint *ids)
{
  (*createFramebuffers)(n, ids);
}

void MisakaFunctions::glNamedFramebufferTexture(GLuint framebuffer, GLenum attachment, GLuint texture, GLint level)
{
  (*namedFramebufferTexture)(framebuffer, attachment, texture, level);
}

void MisakaFunctions::glCreateRenderbuffers(GLsizei n, GLuint *renderbuffers)
{
  (*createRenderbuffers)(n, renderbuffers);
}

void MisakaFunctions::glNamedRenderbufferStorage(GLuint renderbuffer, GLenum internalformat, GLsizei width, GLsizei height)
{
  (*namedRenderbufferStorage)(renderbuffer, internalformat, width, height);
}

void MisakaFunctions::glNamedFramebufferRenderbuffer(GLuint framebuffer, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer)
{
  (*namedFramebufferRenderbuffer)(framebuffer, attachment, renderbuffertarget, renderbuffer);
}

void MisakaFunctions::glTextureParameteri(GLuint texture, GLenum pname, GLint param)
{
  (*textureParameteri)(texture, pname, param);
}

void MisakaFunctions::glTextureParameterf(GLuint texture, GLenum pname, GLfloat param)
{
  (*textureParameterf)(texture, pname, param);
}

void MisakaFunctions::glGenerateTextureMipmap(GLuint texture)
{
  (*generateTextureMipmap)(texture);
}

void MisakaFunctions::setTextureAnisotropy(GLuint texture)
{
  glTextureParameterf(texture, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAnisotropy);
}

void MisakaFunctions::setTextureAnisotropy(GLuint texture, GLfloat factor)
{
  glTextureParameterf(texture, GL_TEXTURE_MAX_ANISOTROPY_EXT, std::min(factor, maxAnisotropy));
}

void MisakaFunctions::textureStorage(GLuint texture, MisakaTextureType type, const glm::uvec4 &sz)
{
  switch (type) {
    case MisakaTextureType::Texture1D: //NotImplemented
    case MisakaTextureType::Texture2D_Multisample: //NotImplemented
//      glTextureStorage2DMultisample(id, samples, GL_RGBA8, sz.x, sz.y, isSampleLocationsFixed);
    case MisakaTextureType::Texture2D_Multisample_Array: //NotImplemented
    case MisakaTextureType::TextureBuffer: //NotAvaliable
    case MisakaTextureType::Invalid: //NotValid
      return;

    case MisakaTextureType::Texture2D:
    case MisakaTextureType::Texture1D_Array:
    case MisakaTextureType::TextureRectangle:
      glTextureStorage2D(texture, sz.w, GL_RGBA8, sz.x, sz.y);
      return;

    case MisakaTextureType::Texture3D:
    case MisakaTextureType::Texture2D_Array:
      glTextureStorage3D(texture, sz.w, GL_RGBA8, sz.x, sz.y, sz.z);
      return;

    case MisakaTextureType::TextureCube:
      glTextureStorage3D(texture, sz.w, GL_RGBA8, sz.x, sz.y, 6);
      return;

    case MisakaTextureType::TextureCube_Array:
      glTextureStorage3D(texture, sz.w, GL_RGBA8, sz.x, sz.y, 6 * sz.z);
      return;
    }
}

void MisakaFunctions::initialize()
{
  BaseGLFunc::initializeOpenGLFunctions();

  auto ctx = QOpenGLContext::currentContext();
  auto getProcAddress = [ctx](auto &proc, const char *procName) {
      proc = reinterpret_cast<typename std::remove_reference_t<decltype(proc)>>(ctx->getProcAddress(procName));
    };

  auto glExtLst = ctx->extensions();
  if(glExtLst.contains("GL_ARB_bindless_texture"))
    {
      getProcAddress(isImageHandleResident,       "glIsImageHandleResidentARB");
      getProcAddress(isTextureHandleResident,     "glIsTextureHandleResidentARB");
      getProcAddress(programUniformHandleui64v,   "glProgramUniformHandleui64vARB");
      getProcAddress(programUniformHandleui64,    "glProgramUniformHandleui64ARB");
      getProcAddress(uniformHandleui64v,          "glUniformHandleui64vARB");
      getProcAddress(uniformHandleui64,           "glUniformHandleui64ARB");
      getProcAddress(makeImageHandleNonResident,  "glMakeImageHandleNonResidentARB");
      getProcAddress(makeImageHandleResident,     "glMakeImageHandleResidentARB");
      getProcAddress(getImageHandle,              "glGetImageHandleARB");
      getProcAddress(makeTextureHandleNonResident,"glMakeTextureHandleNonResidentARB");
      getProcAddress(makeTextureHandleResident,   "glMakeTextureHandleResidentARB");
      getProcAddress(getTextureSamplerHandle,     "glGetTextureSamplerHandleARB");
      getProcAddress(getTextureHandle,            "glGetTextureHandleARB");
    }

  nvShaders5 = glExtLst.contains("GL_NV_gpu_shader5");
  if(nvShaders5)
    gpuShader5 = "#extension GL_NV_gpu_shader5: enable\n";

  if(glExtLst.contains("GL_ARB_gpu_shader5"))
    gpuShader5 += "#extension GL_ARB_gpu_shader5: enable\n";

  getProcAddress(createTextures, "glCreateTextures");
  getProcAddress(textureStorage1D, "glTextureStorage1D");
  getProcAddress(textureStorage2D, "glTextureStorage2D");
  getProcAddress(textureStorage3D, "glTextureStorage3D");
  getProcAddress(textureSubImage1D, "glTextureSubImage1D");
  getProcAddress(textureSubImage2D, "glTextureSubImage2D");
  getProcAddress(textureSubImage3D, "glTextureSubImage3D");

  getProcAddress(createBuffers, "glCreateBuffers");
  getProcAddress(namedBufferStorage, "glNamedBufferStorage");
  getProcAddress(namedBufferSubData, "glNamedBufferSubData");

  getProcAddress(createVertexArrays, "glCreateVertexArrays");
  getProcAddress(vertexArrayVertexBuffer, "glVertexArrayVertexBuffer");
  getProcAddress(enableVertexArrayAttrib, "glEnableVertexArrayAttrib");
  getProcAddress(disableVertexArrayAttrib, "glDisableVertexArrayAttrib");
  getProcAddress(vertexArrayAttribFormat, "glVertexArrayAttribFormat");
  getProcAddress(vertexArrayAttribBinding, "glVertexArrayAttribBinding");
  getProcAddress(vertexArrayElementBuffer, "glVertexArrayElementBuffer");

  getProcAddress(createFramebuffers, "glCreateFramebuffers");
  getProcAddress(namedFramebufferTexture, "glNamedFramebufferTexture");
  getProcAddress(createRenderbuffers, "glCreateRenderbuffers");
  getProcAddress(namedRenderbufferStorage, "glNamedRenderbufferStorage");
  getProcAddress(namedFramebufferRenderbuffer, "glNamedFramebufferRenderbuffer");

  getProcAddress(textureParameteri, "glTextureParameteri");
  getProcAddress(textureParameterf, "glTextureParameterf");
  getProcAddress(generateTextureMipmap, "glGenerateTextureMipmap");

  glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAnisotropy);
}
