#ifndef MISAKAMESHINSTANCEIMPL_HPP
#define MISAKAMESHINSTANCEIMPL_HPP

#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include "../MisakaGpuBufImpl.hpp"
#include <qopengl.h>

class KawaiiMeshInstance;
class KawaiiMesh3D;
class MisakaRootImpl;
class MisakaVao3D;

class MISAKA3D_SHARED_EXPORT MisakaMeshInstanceImpl: public KawaiiRendererImpl
{
  Q_OBJECT
  friend class MisakaMeshImpl;

public:
  MisakaMeshInstanceImpl(KawaiiMeshInstance *model);
  ~MisakaMeshInstanceImpl();

  void draw();



  //IMPLEMENT
private:
  KawaiiMeshInstance *model;
  MisakaVao3D *vao;

  MisakaGpuBufImpl *getUniforms() const;
  void setVao(MisakaVao3D *vao);

  void onMeshChanged();

  // KawaiiRendererImpl interface
private:
  void initConnections() override;
};

#endif // MISAKAMESHINSTANCEIMPL_HPP
