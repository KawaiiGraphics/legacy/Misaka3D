#ifndef ORPHANVAO_HPP
#define ORPHANVAO_HPP

#include "../OrphanGLData.hpp"
#include "../Misaka3D_global.hpp"
#include <qopengl.h>

class MISAKA3D_SHARED_EXPORT OrphanVao : public OrphanGLData
{
  GLuint id;
public:
  OrphanVao(GLuint id);
  ~OrphanVao() = default;

  // OrphanGLData interface
public:
  void destroy(MisakaFunctions &gl) override final;
};

#endif // ORPHANVAO_HPP
