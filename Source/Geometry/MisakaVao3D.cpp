#include "MisakaVao3D.hpp"
#include "OrphanVao.hpp"
#include <algorithm>

MisakaVao3D::MisakaVao3D(MisakaContext3D *parent) : MisakaGLObject(parent),
  baseAttr(MisakaGLBufferType::VBO, this, this),
  indBuf(MisakaGLBufferType::VertexArrayIndices, this, this),
  indBufCtrl(&indBuf),
  count(0),
  minIndex(std::numeric_limits<GLuint>::min()),
  maxIndex(std::numeric_limits<GLuint>::max())
{
  baseAttr.setObjectName("Base VBO");
  indBuf.setObjectName("EBO");

  connect(&baseAttr, &MisakaGLObject::initialized, this, &MisakaVao3D::onBaseAttrReinit);
  connect(&indBuf, &MisakaGLObject::initialized, this, &MisakaVao3D::onIndBufReinit);
}

MisakaVao3D::~MisakaVao3D()
{
}

void MisakaVao3D::draw()
{
  gl().glBindVertexArray(ID());
  gl().glDrawRangeElements(GL_TRIANGLES, minIndex, maxIndex, count, GL_UNSIGNED_INT, nullptr);
}

void MisakaVao3D::drawInstanced(GLsizei instances)
{
  gl().glBindVertexArray(ID());
  gl().glDrawElementsInstanced(GL_TRIANGLES, count, GL_UNSIGNED_INT, nullptr, instances);
}

void MisakaVao3D::setIndices(const QVector<KawaiiTriangle3D> &indices)
{
  indBufCtrl.setData(indices);
}

void MisakaVao3D::updateIndex(int i)
{
  indBufCtrl.updateDataElements<uint32_t>(i, 1);
}

void MisakaVao3D::initialize(GLuint &id)
{
  gl().glCreateVertexArrays(1, &id);
  setOrphanHandle(new OrphanVao(id));

  static constexpr GLuint posInd = 0, normInd = 1, texCInd = 2,
      globalOffset = 0;

  baseAttr.bindVertexBuffer(id, 0, globalOffset, sizeof(KawaiiPoint3D));
  indBuf.bindElementBuffer(id);

  gl().glEnableVertexArrayAttrib(id, posInd);
  gl().glVertexArrayAttribFormat(id, posInd, 4, MisakaGLVariableType::Float, false, 0);
  gl().glVertexArrayAttribBinding(id, posInd, 0);

  gl().glEnableVertexArrayAttrib(id, normInd);
  gl().glVertexArrayAttribFormat(id, normInd, 3, MisakaGLVariableType::Float, false, sizeof(QVector4D));
  gl().glVertexArrayAttribBinding(id, normInd, 0);

  gl().glEnableVertexArrayAttrib(id, texCInd);
  gl().glVertexArrayAttribFormat(id, texCInd, 3, MisakaGLVariableType::Float, false, sizeof(QVector4D)+sizeof(QVector3D));
  gl().glVertexArrayAttribBinding(id, texCInd, 0);
}

void MisakaVao3D::updateNow()
{}

void MisakaVao3D::onBaseAttrReinit()
{
  if(isValid())
    baseAttr.bindVertexBuffer(ID(), 0, 0, sizeof(KawaiiPoint3D));
}

void MisakaVao3D::onIndBufReinit()
{
  count = indBufCtrl.getElemetsCount<GLuint>();

  const GLuint *a = indBufCtrl.getBuffer()->getData<GLuint>();
  const auto minmax = std::minmax_element(a, a+count);
  minIndex = *minmax.first;
  maxIndex = *minmax.second;

  if(isValid())
    indBuf.bindElementBuffer(ID());
}
