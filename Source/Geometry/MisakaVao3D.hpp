#ifndef MISAKAVAO3D_HPP
#define MISAKAVAO3D_HPP

#include "../MisakaBufferImpl.hpp"
#include <Kawaii3D/Geometry/KawaiiPoint3D.hpp>
#include <Kawaii3D/Geometry/KawaiiTriangle3D.hpp>

class MISAKA3D_SHARED_EXPORT MisakaVao3D : public MisakaGLObject
{
  Q_OBJECT
public:
  explicit MisakaVao3D(MisakaContext3D *parent = nullptr);
  ~MisakaVao3D();

  inline MisakaGLBuffer& getBaseAttrBuf() { return baseAttr; }
  inline const MisakaGLBuffer& getBaseAttrBuf() const { return baseAttr; }

  inline MisakaGLBuffer& getIndBuf() { return indBuf; }
  inline const MisakaGLBuffer& getIndBuf() const { return indBuf; }

  void draw();
  void drawInstanced(GLsizei instances);

  void setIndices(const QVector<KawaiiTriangle3D> &indices);
  void updateIndex(int i);

  // MisakaGLObject interface
protected:
  void initialize(GLuint &id) override;
  void updateNow() override;



  //IMPLEMENT
private:
  MisakaGLBuffer baseAttr;
  MisakaGLBuffer indBuf;

  MisakaBufferImpl indBufCtrl;

  GLsizei count;
  GLuint minIndex;
  GLuint maxIndex;

  void onBaseAttrReinit();
  void onIndBufReinit();
};

#endif // MISAKAVAO3D_HPP
