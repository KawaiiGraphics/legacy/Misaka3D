#include "OrphanVao.hpp"
#include "MisakaFunctions.hpp"

OrphanVao::OrphanVao(GLuint id):
  id(id)
{
}

void OrphanVao::destroy(MisakaFunctions &gl)
{
  gl.glDeleteVertexArrays(1, &id);
}
