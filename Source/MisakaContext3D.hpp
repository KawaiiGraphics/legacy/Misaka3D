#ifndef MisakaContext3D_HPP
#define MisakaContext3D_HPP

#include <QObject>
#include <QVector>
#include <QPointer>
#include <QLinkedList>
#include <QOpenGLContext>
#include "MisakaFunctions.hpp"
#include "OrphanGLData.hpp"
#include "MisakaDirtyGLObjects.hpp"

class MisakaGLObject;
class MisakaFbo;

class MISAKA3D_SHARED_EXPORT MisakaContext3D : public QObject
{
  Q_OBJECT
  friend class MisakaRootImpl;
  friend class MisakaGLObject;

public:
  class OffscreenRenderIterator {
    int index;
    QPointer<MisakaContext3D> ctx;
    friend class ::MisakaContext3D;

  public:
    OffscreenRenderIterator(MisakaContext3D *ctx, int index);
    OffscreenRenderIterator();
    ~OffscreenRenderIterator();

    inline bool isValid() const
    { return ctx; }

    std::function<void()>& value() const;
  };

  explicit MisakaContext3D(QObject *parent = nullptr);
  virtual ~MisakaContext3D();

  static QSurfaceFormat &requestedFormat();
  static QSurfaceFormat &requestedFormat(QSurfaceFormat &&original);

  void beginDraw(QSurface *sfc);
  void setViewport(const QRect &viewport);
  void endDraw(QSurface *sfc);

  ///Returns iteraror to added hook
  OffscreenRenderIterator addOffscreenRender(std::function<void()> &&hook);

  ///Returns iteraror to added hook
  OffscreenRenderIterator addOffscreenRender(std::function<void()> &hook);

  void removeOffscreenRender(const OffscreenRenderIterator &pos);

  MisakaFbo *getOffscreenRender();

  static bool checkSystem();



  //IMPLEMENT
private:
  QOpenGLContext d;
  MisakaFunctions gl;
  QVector<MisakaGLObject*> glObjects;
  MisakaDirtyGLObjects dirtyObjects;
  QLinkedList<OrphanGLData*> orphan;

  QPointer<MisakaFbo> offscreenFbo;
  std::vector<std::function<void()>> offscrRenderHooks;
  QLinkedList<OffscreenRenderIterator*> offscrIterators;

  GLuint defaultFbo;

  void deleteOffscreenRender();
};

#endif // MisakaContext3D_HPP
