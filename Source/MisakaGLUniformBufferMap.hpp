#ifndef MISAKAGLUNIFORMBUFFERMAP_HPP
#define MISAKAGLUNIFORMBUFFERMAP_HPP

#include "MisakaGLObject.hpp"
#include <memory>

class MisakaGLUniformBufferMap : public MisakaGLObject
{
  Q_OBJECT

public:
  template<typename... ArgsT>
  inline MisakaGLUniformBufferMap(ArgsT&&... args):
    MisakaGLObject(std::forward<ArgsT>(args)...),
    realID(0),
    askedSize(0),
    used(false)
  {}

  void querrySetUboField(const QString &field, const void *ptr, size_t n);

  void bindToBlock(GLuint bindingPoint);
  void unbindBlock(GLuint bindingPoint);

  // MisakaGLObject interface
private:
  void initialize(GLuint &id) override;
  void updateNow() override;



  //IMPLEMENT
  struct FieldSetQuerry {
    QString name;
    size_t sz;
    void *mem;

    FieldSetQuerry(QString name, const void *mem, size_t sz):
      name(name),
      sz(sz),
      mem(malloc(sz))
    {
      memcpy(this->mem, mem, sz);
    }

    FieldSetQuerry(const FieldSetQuerry &another) = delete;

    ~FieldSetQuerry()
    {
      free(mem);
    }
  };

  QLinkedList<std::shared_ptr<FieldSetQuerry>> uboSetQuerry;
  GLuint realID;
  GLint askedSize;
  bool used;

  void createUbo(GLint prog, GLuint bindingPoint);
};

#endif // MISAKAGLUNIFORMBUFFERMAP_HPP
