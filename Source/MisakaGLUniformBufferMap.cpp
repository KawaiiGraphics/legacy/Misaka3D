#include "MisakaGLBufferType.hpp"
#include "MisakaGLUniformBufferMap.hpp"
#include "OrphanGLBuffer.hpp"
#include <Kawaii3D/Shaders/KawaiiShader.hpp>

void MisakaGLUniformBufferMap::querrySetUboField(const QString &field, const void *ptr, size_t n)
{
  for(auto &i: uboSetQuerry)
    if(i->name == field)
      {
        i = std::make_shared<FieldSetQuerry>(field, ptr, n);
        return;
      }
  uboSetQuerry.push_back(std::make_shared<FieldSetQuerry>(field, ptr, n));
}

void MisakaGLUniformBufferMap::bindToBlock(GLuint bindingPoint)
{
  bindingPoint += KawaiiShader::getUboCount();
  if(!uboSetQuerry.isEmpty())
    {
      GLint prog;
      gl().glGetIntegerv(GL_CURRENT_PROGRAM, &prog);
      if(prog)
        {
          if(!used)
            createUbo(prog, bindingPoint);

          std::vector<GLchar*> uniformNames(uboSetQuerry.size());
          auto namIter = uniformNames.begin();

          for(const auto &i: uboSetQuerry)
            {
              const QByteArray buf = i->name.toLatin1();
              *namIter = static_cast<GLchar*>(std::malloc(buf.size()+1));
              memcpy(*namIter, buf.constData(), buf.size());
              memset((*namIter) + buf.size(), 0, 1);
              ++namIter;
            }

          std::vector<GLuint> indices(uboSetQuerry.size());
          gl().glGetUniformIndices(prog, uboSetQuerry.size(), &uniformNames[0], &indices[0]);

          for(GLchar *ch: uniformNames)
            std::free(ch);

          std::vector<GLint> offsets(uboSetQuerry.size());
          gl().glGetActiveUniformsiv(prog, uboSetQuerry.size(), &indices[0], GL_UNIFORM_OFFSET, &offsets[0]);


          auto offsetsIter = offsets.begin();
          //Now we can update waiting fields
          while(!uboSetQuerry.isEmpty())
            {
              auto querry = uboSetQuerry.takeFirst();
              gl().glNamedBufferSubData(realID, *(offsetsIter++), querry->sz, querry->mem);
            }
        }
    }
  gl().glBindBufferBase(MisakaGLBufferType::UBO, bindingPoint, realID);
}

void MisakaGLUniformBufferMap::unbindBlock(GLuint bindingPoint)
{
  gl().glBindBufferBase(MisakaGLBufferType::UBO, bindingPoint, 0);
}

void MisakaGLUniformBufferMap::initialize(GLuint &)
{ }

void MisakaGLUniformBufferMap::updateNow()
{ }

void MisakaGLUniformBufferMap::createUbo(GLint prog, GLuint bindingPoint)
{
  GLuint uniformBlockIndex = 0;
  while(uniformBlockIndex <= 2 * KawaiiShader::getUboCount())
    {
      GLint tempInd = 0;
      gl().glGetActiveUniformBlockiv(prog, uniformBlockIndex, GL_UNIFORM_BLOCK_BINDING, &tempInd);
      if(tempInd > 0 && static_cast<GLuint>(tempInd) == bindingPoint)
        break;
      ++uniformBlockIndex;
    }

  askedSize = 0;
  gl().glGetActiveUniformBlockiv(prog, uniformBlockIndex, GL_UNIFORM_BLOCK_DATA_SIZE, &askedSize);

  if(askedSize > 0)
    {
      gl().glCreateBuffers(1, &realID);
      gl().glNamedBufferStorage(realID, askedSize, nullptr, GL_DYNAMIC_STORAGE_BIT);
      setOrphanHandle(new OrphanGLBuffer(realID));
      used = true;
    }
}
