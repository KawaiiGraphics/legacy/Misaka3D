#ifndef MISAKACAMERAIMPL_HPP
#define MISAKACAMERAIMPL_HPP

#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include <Kawaii3D/KawaiiCamera.hpp>
#include "Misaka3D_global.hpp"
#include "MisakaGpuBufImpl.hpp"

class MisakaRootImpl;
class MisakaContext3D;
class MisakaSceneImpl;

class MISAKA3D_SHARED_EXPORT MisakaCameraImpl : public KawaiiRendererImpl
{
  Q_OBJECT

public:
  MisakaCameraImpl(KawaiiCamera *model);
  ~MisakaCameraImpl();

  void draw(MisakaGLBuffer *sfcUbo);

  KawaiiCamera *getModel() const;
  glm::mat4 getViewProjectionMat(const glm::mat4 &projMat) const;



  //IMPLEMENT
private:
  KawaiiCamera *model;
  MisakaGpuBufImpl* ubo() const;
  MisakaSceneImpl* scene() const;
};

#endif // MISAKACAMERAIMPL_HPP
