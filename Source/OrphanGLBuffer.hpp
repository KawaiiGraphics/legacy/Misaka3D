#ifndef ORPHANGLBUFFER_HPP
#define ORPHANGLBUFFER_HPP

#include <qopengl.h>
#include "Misaka3D_global.hpp"
#include "OrphanGLData.hpp"

class MISAKA3D_SHARED_EXPORT OrphanGLBuffer : public OrphanGLData
{
  GLuint id;
public:
  OrphanGLBuffer(GLuint id);
  ~OrphanGLBuffer() = default;

  // OrphanGLData interface
public:
  void destroy(MisakaFunctions &gl) override final;
};

#endif // ORPHANGLBUFFER_HPP
