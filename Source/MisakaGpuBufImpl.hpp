#ifndef MisakaGpuBufIMPL_HPP
#define MisakaGpuBufIMPL_HPP

#include "MisakaBufferImpl.hpp"

class KawaiiGpuBuf;
class MisakaRootImpl;
class MisakaGLUniformBufferMap;

class MISAKA3D_SHARED_EXPORT MisakaGpuBufImpl: public MisakaBufferImpl
{
  Q_OBJECT
public:
  MisakaGpuBufImpl(KawaiiGpuBuf *model);
  ~MisakaGpuBufImpl();

  void querrySetField(const QString &fieldName, const void *memPtr, size_t memSize);

  template<typename T>
  void querrySetField(const QString &fieldName, const T &field)
  { querrySetField(fieldName, &field, sizeof(T)); }

  void bind(GLuint blockIndex, MisakaGLBufferType target = MisakaGLBufferType::UBO) const;
  void unbind(GLuint blockIndex, MisakaGLBufferType target = MisakaGLBufferType::UBO) const;

  KawaiiGpuBuf* getModel() const;



  //IMPLEMENT
private:
  QPointer<MisakaGLUniformBufferMap> advBuf;
  KawaiiGpuBuf *model;

  MisakaRootImpl *root;

  void createBuffer();
  void deleteView();
  void setupBuffer();

  void onDataParentChanged();
};

#endif // MisakaGpuBufIMPL_HPP
