#include "Shaders/MisakaSceneImpl.hpp"
#include "MisakaCameraImpl.hpp"
#include "MisakaRootImpl.hpp"
#include <Kawaii3D/KawaiiRoot.hpp>
#include <QEvent>

MisakaCameraImpl::MisakaCameraImpl(KawaiiCamera *model):
  KawaiiRendererImpl(model),
  model(model)
{
  if(!model)
    deleteLater();
}

MisakaCameraImpl::~MisakaCameraImpl()
{
}

void MisakaCameraImpl::draw(MisakaGLBuffer *sfcUbo)
{
  if(scene() && ubo())
    scene()->draw(ubo()->getBuffer(), sfcUbo);
}

KawaiiCamera *MisakaCameraImpl::getModel() const
{
  return model;
}

glm::mat4 MisakaCameraImpl::getViewProjectionMat(const glm::mat4 &projMat) const
{
  if(model)
    return model->getViewProjectionMat(projMat);
  else
    return projMat;
}

MisakaGpuBufImpl *MisakaCameraImpl::ubo() const
{
  return static_cast<MisakaGpuBufImpl*>(model->getUniforms()->getRendererImpl());
}

MisakaSceneImpl *MisakaCameraImpl::scene() const
{
  if(model->getScene())
    return static_cast<MisakaSceneImpl*>(model->getScene()->getRendererImpl());
  else
    return nullptr;
}
