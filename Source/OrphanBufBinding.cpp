#include <Kawaii3D/Shaders/KawaiiShader.hpp>
#include "OrphanBufBinding.hpp"
#include "MisakaGpuBufImpl.hpp"

OrphanBufBinding::OrphanBufBinding(MisakaGpuBufImpl *buffer, GLuint bindingPoint, MisakaGLBufferType target):
  buffer(buffer),
  bindingPoint(bindingPoint),
  target(target)
{
  onTargetDestroyed = QObject::connect(buffer, &QObject::destroyed, [this] { this->buffer = nullptr; });
}

void OrphanBufBinding::destroy(MisakaFunctions &gl)
{
  static_cast<void>(gl);
  if(buffer)
    buffer->unbind(bindingPoint + 2 * KawaiiShader::getUboCount(), target);
}
