#include <Kawaii3D/Shaders/KawaiiShader.hpp>
#include "MisakaGLBufBinding.hpp"
#include "OrphanBufBinding.hpp"
#include "MisakaGpuBufImpl.hpp"
#include <QDebug>

void MisakaGLBufBinding::setTarget(KawaiiBufferTarget value)
{
  switch(value)
    {
    case KawaiiBufferTarget::UBO:
      target = MisakaGLBufferType::UBO;
      break;

    case KawaiiBufferTarget::SSBO:
      target = MisakaGLBufferType::SSBO;
      break;

    case KawaiiBufferTarget::AtomicCounter:
      target = MisakaGLBufferType::AtomicCounter;
      break;

    case KawaiiBufferTarget::TransformFeedback:
      target = MisakaGLBufferType::TransformFeedback;
      break;

    default:
      target = MisakaGLBufferType::UBO;
      qWarning() << "MisakaGLBufBinding::setTarget: unsupported binding target! MisakaGLBufferType::UBO was forced!";
      break;
    }
}

MisakaGLBufferType MisakaGLBufBinding::getTarget() const
{
  return target;
}

void MisakaGLBufBinding::initialize(GLuint &id)
{
  id = 1;
  setOrphanHandle(new OrphanBufBinding(buffer, bindingPoint, target));
  buffer->bind(bindingPoint + 2 * KawaiiShader::getUboCount(), target);
}
