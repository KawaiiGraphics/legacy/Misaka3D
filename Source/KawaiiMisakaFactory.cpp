#include "KawaiiMisakaFactory.hpp"

#include <Kawaii3D/KawaiiRoot.hpp>
#include "MisakaRootImpl.hpp"

#include <Kawaii3D/Surfaces/KawaiiSurface.hpp>
#include "Surfaces/MisakaSurfaceImpl.hpp"

#include <Kawaii3D/KawaiiGpuBuf.hpp>
#include "MisakaGpuBufImpl.hpp"

#include <Kawaii3D/KawaiiBufBinding.hpp>
#include "MisakaBufBindingImpl.hpp"

#include <Kawaii3D/Geometry/KawaiiMesh3D.hpp>
#include "Geometry/MisakaMeshImpl.hpp"

#include <Kawaii3D/Shaders/KawaiiProgram.hpp>
#include "Shaders/MisakaProgramImpl.hpp"

#include <Kawaii3D/Shaders/KawaiiShader.hpp>
#include "Shaders/MisakaShaderImpl.hpp"

#include <Kawaii3D/Geometry/KawaiiMeshInstance.hpp>
#include "Geometry/MisakaMeshInstanceImpl.hpp"

#include <Kawaii3D/KawaiiMaterial.hpp>
#include "MisakaMaterialImpl.hpp"

#include <Kawaii3D/Shaders/KawaiiScene.hpp>
#include "Shaders/MisakaSceneImpl.hpp"

#include <Kawaii3D/KawaiiCamera.hpp>
#include "MisakaCameraImpl.hpp"

#include <Kawaii3D/Textures/KawaiiTexture.hpp>
#include "Textures/MisakaTextureImpl.hpp"

#include <Kawaii3D/Textures/KawaiiImage.hpp>
#include "Textures/MisakaImgImpl.hpp"

#include <Kawaii3D/Textures/KawaiiFramebuffer.hpp>
#include "Textures/MisakaFramebufferImpl.hpp"

#include <Kawaii3D/Textures/KawaiiTexBinding.hpp>
#include "Textures/MisakaTexBindingImpl.hpp"

#include <QCoreApplication>

KawaiiMisakaFactory *KawaiiMisakaFactory::instance = nullptr;

KawaiiMisakaFactory::KawaiiMisakaFactory()
{
  registerImpl<KawaiiRoot,         MisakaRootImpl>         ();
  registerImpl<KawaiiSurface,      MisakaSurfaceImpl>      ();
  registerImpl<KawaiiGpuBuf,       MisakaGpuBufImpl>       ();
  registerImpl<KawaiiBufBinding,   MisakaBufBindingImpl>   ();
  registerImpl<KawaiiMesh3D,       MisakaMeshImpl>         ();
  registerImpl<KawaiiProgram,      MisakaProgramImpl>      ();
  registerImpl<KawaiiShader,       MisakaShaderImpl>       ();
  registerImpl<KawaiiMeshInstance, MisakaMeshInstanceImpl> ();
  registerImpl<KawaiiMaterial,     MisakaMaterialImpl>     ();
  registerImpl<KawaiiScene,        MisakaSceneImpl>        ();
  registerImpl<KawaiiCamera,       MisakaCameraImpl>       ();
  registerImpl<KawaiiTexture,      MisakaTextureImpl>      ();
  registerImpl<KawaiiImage,        MisakaImgImpl>          ();
  registerImpl<KawaiiFramebuffer,  MisakaFramebufferImpl>  ();
  registerImpl<KawaiiTexBinding,   MisakaTexBindingImpl>   ();
}

KawaiiImplFactory *KawaiiMisakaFactory::getInstance()
{
  if(!instance)
    createInstance();
  return instance;
}

void KawaiiMisakaFactory::deleteInstance()
{
  if(instance) {
      instance->deleteLater();
      instance = nullptr;
    }
}

void KawaiiMisakaFactory::createInstance()
{
  instance = new KawaiiMisakaFactory;
  QObject::connect(QCoreApplication::instance(), &QCoreApplication::aboutToQuit, &KawaiiMisakaFactory::deleteInstance);
}
