#ifndef MISAKABUFBINDINGIMPL_HPP
#define MISAKABUFBINDINGIMPL_HPP

#include <Kawaii3D/KawaiiBufBinding.hpp>
#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include "MisakaGLBufBinding.hpp"
#include "MisakaGpuBufImpl.hpp"

class MISAKA3D_SHARED_EXPORT MisakaBufBindingImpl: public KawaiiRendererImpl
{
  Q_OBJECT

public:
  MisakaBufBindingImpl(KawaiiBufBinding *model);
  ~MisakaBufBindingImpl();



  //IMPLEMENT
private:
  QPointer<MisakaGLBufBinding> view;
  MisakaGpuBufImpl *buf;
  KawaiiBufBinding *model;
  MisakaRootImpl *root;

  void onDataParentChanged();
  void allocView();
  void createView();
  void updateTarget();
  void updateView();

  void bindImmediately();

  // KawaiiRendererImpl interface
private:
  void initConnections() override;
};

#endif // MISAKABUFBINDINGIMPL_HPP
