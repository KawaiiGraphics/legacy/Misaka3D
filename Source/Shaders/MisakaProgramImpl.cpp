#include <Kawaii3D/Shaders/KawaiiProgram.hpp>
#include <Kawaii3D/KawaiiRoot.hpp>
#include "MisakaProgramImpl.hpp"
#include "MisakaShaderImpl.hpp"
#include "MisakaRootImpl.hpp"
#include "MisakaProgram.hpp"

MisakaProgramImpl::MisakaProgramImpl(KawaiiProgram *model):
  KawaiiRendererImpl(model),
  model(model),
  root(nullptr),
  view(nullptr)
{
  if(model)
    onDataParentChanged();
  else
    deleteLater();
}

MisakaProgramImpl::~MisakaProgramImpl()
{
  if(view)
    view->deleteLater();
}

void MisakaProgramImpl::use() const
{
  if(view)
    view->use();
}

void MisakaProgramImpl::onDataParentChanged()
{
  auto _root = static_cast<MisakaRootImpl*>(KawaiiRoot::getRoot(model->parent())->getRendererImpl());
  if(_root == root) return;

  if(root)
    {
      disconnect(root, &MisakaRootImpl::compiledShader, this, &MisakaProgramImpl::onShaderCompiled);
      disconnect(root, &MisakaRootImpl::removedShader, this, &MisakaProgramImpl::onShaderRemoved);
    }

  root = _root;

  if(root)
    {
      connect(root, &MisakaRootImpl::compiledShader, this, &MisakaProgramImpl::onShaderCompiled);
      connect(root, &MisakaRootImpl::removedShader, this, &MisakaProgramImpl::onShaderRemoved);
    }

  createView();
}

void MisakaProgramImpl::createView()
{
  if(view)
    view->deleteLater();

  if(root)
    {
      view = new MisakaProgram(root->getContext());

      attachShaders(model->getVertexShaders());
      attachShaders(model->getTesselationControllShaders());
      attachShaders(model->getTesselationEvalutionShaders());
      attachShaders(model->getGeometryShaders());
      attachShaders(model->getFragmentShaders());
    } else
    view.clear();
}

void MisakaProgramImpl::attachShaders(const QSet<KawaiiShader*> &shaders)
{
  for(auto i = shaders.begin(); i != shaders.end(); ++i)
    {
      auto el = static_cast<MisakaShaderImpl*>((*i)->getRendererImpl());
      if(el)
        {
          bool ok;
          GLuint id = el->ID(&ok);
          if(ok)
            view->attachShader(id);
        }
    }
}

void MisakaProgramImpl::onShaderCompiled(KawaiiShader *shader, GLuint id)
{
  if(model->usesShader(shader))
    view->attachShader(id);
}

void MisakaProgramImpl::onShaderRemoved(KawaiiShader *shader, GLuint id)
{
  if(model->usesShader(shader))
    view->detachShader(id);
}
