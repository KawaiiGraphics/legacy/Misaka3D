#ifndef MISAKASHADERIMPL_HPP
#define MISAKASHADERIMPL_HPP

#include <Kawaii3D/Shaders/KawaiiShader.hpp>
#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include "MisakaShader.hpp"
#include <QPointer>

class MisakaRootImpl;
class MisakaProgramImpl;

class MISAKA3D_SHARED_EXPORT MisakaShaderImpl : public KawaiiRendererImpl
{
  Q_OBJECT
public:
  explicit MisakaShaderImpl(KawaiiShader *model);
  ~MisakaShaderImpl();

  bool exists() const;
  GLuint ID(bool *ok) const;



  //IMPLEMENT
private:
  KawaiiShader *model;
  MisakaRootImpl *root;
  MisakaProgramImpl *prog;
  QPointer<MisakaShader> view;

  void onDataParentChanged();
  void createView();
  void updateGLSL();
  void onViewRecompiled();

  // KawaiiRendererImpl interface
private:
  void initConnections() override;
};

#endif // MISAKASHADERIMPL_HPP
