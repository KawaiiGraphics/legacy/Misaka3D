﻿#include <QRegularExpression>
#include "MisakaProgram.hpp"
#include "MisakaRootImpl.hpp"
#include "MisakaShaderImpl.hpp"
#include "MisakaProgramImpl.hpp"
#include <sib_utils/Strings.hpp>
#include <Kawaii3D/KawaiiRoot.hpp>
#include <Kawaii3D/Shaders/KawaiiProgram.hpp>

MisakaShaderImpl::MisakaShaderImpl(KawaiiShader *model):
  KawaiiRendererImpl(model),
  model(model),
  root(nullptr),
  prog(nullptr),
  view(nullptr)
{
  if(model)
    {
      connect(model, &KawaiiShader::reparsed, this, &MisakaShaderImpl::updateGLSL);
      connect(model, &KawaiiShader::parentUpdated, this, &MisakaShaderImpl::onDataParentChanged);

      onDataParentChanged();
    } else
    deleteLater();
}

MisakaShaderImpl::~MisakaShaderImpl()
{
  if(view)
    view->deleteLater();
}

bool MisakaShaderImpl::exists() const
{
  return view && view->isValid();
}

GLuint MisakaShaderImpl::ID(bool *ok) const
{
  bool valid = exists();

  if(ok)
    *ok = valid;

  return valid? view->ID(): 0;
}

void MisakaShaderImpl::onDataParentChanged()
{
  auto _root = MisakaRootImpl::findRootImpl(this);
  if(_root != root)
    {
      bool shaderExists;
      GLuint id = ID(&shaderExists);
      if(root && shaderExists)
        root->removedShader(model, id);

      if(view)
        view->deleteLater();

      root = _root;
      createView();
    }
}

void MisakaShaderImpl::createView()
{
  bool shaderExists;
  GLuint id = ID(&shaderExists);
  if(root && shaderExists)
    root->removedShader(model, id);

  if(view)
    view->deleteLater();

  if(prog)
    view = new MisakaShader(getMisakaShaderType(model->getType()), prog->view, prog->view);
  else
    {
      if(model->getProgram())
        return;

      if(root)
        view = new MisakaShader(getMisakaShaderType(model->getType()), root->getContext(), root->getContext());
      else
        view = nullptr;
    }

  if(!view) return;

  updateGLSL();
  connect(view, &MisakaShader::shaderRecompiled, this, &MisakaShaderImpl::onViewRecompiled);
}

void MisakaShaderImpl::updateGLSL()
{
  if(!view) return;

  QString glsl = model->getCode();

  QMap<int, QString> injections;

  bool nvidiaShaders = view->gl().nvidiaShaders5();
  auto injectTextureHandles = [&injections, nvidiaShaders](int blockEnd, const QString &blockName, int blockIndex, const QHash<QString, QString> &textures) {
      if(textures.isEmpty())
        return;
      QString block = QString("\nlayout(shared, binding = %1) uniform %2\n{\n").arg(KawaiiShader::getUboCount() + blockIndex).arg(blockName),
          global;

      using sib_utils::strings::getQStr;

      const QString globTerm = nvidiaShaders?
            getQStr("#define %1 (%2(%3))\n"):
            getQStr("%2 %1 = %2(%3);\n");

      for(auto i = textures.begin(); i != textures.end(); ++i)
        {
          QString handleName = i.key() + "_handle";
          block += "    uvec2 " + handleName + ";\n";
          auto components = i->split(' ');
          global += QString(globTerm).arg(components.last(), components.first(), handleName);
        }
      block += "};\n" + global;
      injections.insert(blockEnd+1, block);
    };

  injectTextureHandles(model->getCameraBlockEnd(), "SIB_CAMERA_TEXTURES", KawaiiShader::getCameraUboLocation(), model->getCameraTextures());
  injectTextureHandles(model->getSurfaceBlockEnd(), "SIB_SURFACE_TEXTURES", KawaiiShader::getSurfaceUboLocation(), model->getSurfaceTextures());
  injectTextureHandles(model->getMaterialBlockEnd(), "SIB_MATERIAL_TEXTURES", KawaiiShader::getMaterialUboLocation(), model->getMaterialTextures());
  injectTextureHandles(model->getModelBlockEnd(), "SIB_MODELS_TEXTURES", KawaiiShader::getModelUboLocation(), model->getModelTextures());

  for(auto i = injections.end()-1; i != injections.begin()-1; --i)
    glsl.insert(i.key(), i.value());

  static QRegularExpression regEx("#version\\s+\\d+\\s+core\\s*");
  auto versionMatch = regEx.match(glsl);
  if(versionMatch.hasMatch())
    {
      if(!injections.isEmpty())
        {
          int i = versionMatch.capturedEnd();
          glsl.insert(i, "\n#extension GL_ARB_bindless_texture: require\n"
                      + view->gl().getShader5GlslExtension() + "\nlayout(bindless_sampler) uniform;\n");
        }
    } else
    {
      const QString prefix = injections.isEmpty()?
            "#version 450 core\n" + view->gl().getShader5GlslExtension():
            "#version 450 core\n#extension GL_ARB_bindless_texture: require\n"
            + view->gl().getShader5GlslExtension() + "\nlayout(bindless_sampler) uniform;\n";
      glsl = prefix + glsl;
    }

  static QRegularExpression exp("GLOBAL_UBO_BINDING_(\\d+)");
  glsl.replace(exp, QString("(\\1 + %1)").arg(2*KawaiiShader::getUboCount()));
  view->setGlsl(glsl);
}

void MisakaShaderImpl::onViewRecompiled()
{
  if(prog)
    prog->view->attachShader(view->ID());
  else
    if(root)
      root->compiledShader(model, view->ID());
}

void MisakaShaderImpl::initConnections()
{
  if(model->getProgram())
    {
      prog = static_cast<MisakaProgramImpl*>(model->getProgram()->getRendererImpl());
      createView();
    } else
    prog = nullptr;
}
