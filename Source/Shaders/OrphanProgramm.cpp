#include "OrphanProgramm.hpp"
#include "MisakaFunctions.hpp"

OrphanProgramm::OrphanProgramm(GLuint id):
  id(id)
{ }

void OrphanProgramm::destroy(MisakaFunctions &gl)
{
  gl.glDeleteProgram(id);
}
