#ifndef ORPHANPROGRAMM_HPP
#define ORPHANPROGRAMM_HPP

#include "../OrphanGLData.hpp"
#include "../Misaka3D_global.hpp"
#include <qopengl.h>

class MISAKA3D_SHARED_EXPORT OrphanProgramm: public OrphanGLData
{
  GLuint id;
public:
  OrphanProgramm(GLuint id);
  ~OrphanProgramm() = default;

  // OrphanGLData interface
public:
  void destroy(MisakaFunctions &gl) override;
};

#endif // ORPHANPROGRAMM_HPP
