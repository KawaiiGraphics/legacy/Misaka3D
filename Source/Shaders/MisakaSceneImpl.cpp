#include "MisakaSceneImpl.hpp"
#include "MisakaRootImpl.hpp"
#include "MisakaMaterialImpl.hpp"
#include "MisakaProgramImpl.hpp"
#include "Geometry/MisakaMeshInstanceImpl.hpp"
#include "MisakaGLBuffer.hpp"
#include <Kawaii3D/Shaders/KawaiiScene.hpp>
#include <Kawaii3D/KawaiiRoot.hpp>

MisakaSceneImpl::MisakaSceneImpl(KawaiiScene *model):
  KawaiiRendererImpl(model),
  model(model)
{
  if(!model)
    deleteLater();
}

MisakaSceneImpl::~MisakaSceneImpl()
{
}

void MisakaSceneImpl::draw(MisakaGLBuffer *cameraUBO, MisakaGLBuffer *surfaceUBO)
{
  //Bind ubo
  cameraUBO->bindToBlock(KawaiiShader::getCameraUboLocation());
  surfaceUBO->bindToBlock(KawaiiShader::getSurfaceUboLocation());

  for(auto i = model->getScene().constBegin(); i != model->getScene().constEnd(); ++i)
    if(i->shaderProg && !i->instances.isEmpty())
      {
        auto progImpl = static_cast<MisakaProgramImpl*>(i->shaderProg->getRendererImpl());
        if(!progImpl)
          continue;

        //Use shader programm
        progImpl->use();

        //Bind material
        if(i.key())
          if(auto materialImpl = static_cast<MisakaMaterialImpl*>(i.key()->getRendererImpl()); materialImpl)
            materialImpl->bind();

        //Draw
        for(auto mesh: i->instances)
          if(auto meshImpl = static_cast<MisakaMeshInstanceImpl*>(mesh->getRendererImpl()); meshImpl)
            meshImpl->draw();
      }
}
