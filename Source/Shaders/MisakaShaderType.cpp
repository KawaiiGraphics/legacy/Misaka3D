#include "MisakaShaderType.hpp"

MisakaShaderType getMisakaShaderType(KawaiiShaderType type)
{
  switch(type)
    {
    case KawaiiShaderType::Compute:
      return MisakaShaderType::Compute;

    case KawaiiShaderType::Vertex:
      return MisakaShaderType::Vertex;

    case KawaiiShaderType::TesselationControll:
      return MisakaShaderType::TessCtrl;

    case KawaiiShaderType::TesselationEvaluion:
      return MisakaShaderType::TessEvalution;

    case KawaiiShaderType::Geometry:
      return MisakaShaderType::Geometry;

    case KawaiiShaderType::Fragment:
      return MisakaShaderType::Fragment;

    default:
      return MisakaShaderType::Invalid;
    }
}
