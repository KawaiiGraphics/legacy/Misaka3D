#ifndef MISAKAPROGRAMIMPL_HPP
#define MISAKAPROGRAMIMPL_HPP

#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include "../Misaka3D_global.hpp"
#include <qopengl.h>
#include <QPointer>

class MisakaRootImpl;
class MisakaProgram;

class KawaiiProgram;
class KawaiiShader;

class MISAKA3D_SHARED_EXPORT MisakaProgramImpl : public KawaiiRendererImpl
{
  Q_OBJECT
  friend class MisakaShaderImpl;
public:
  explicit MisakaProgramImpl(KawaiiProgram *model);
  ~MisakaProgramImpl();

  void use() const;



  //IMPLEMENT
private:
  KawaiiProgram *model;
  MisakaRootImpl *root;
  QPointer<MisakaProgram> view;

  void onDataParentChanged();

  void createView();
  void attachShaders(const QSet<KawaiiShader*> &shaders);

  void onShaderCompiled(KawaiiShader *shader, GLuint id);
  void onShaderRemoved(KawaiiShader *shader, GLuint id);
};

#endif // MISAKAPROGRAMIMPL_HPP
