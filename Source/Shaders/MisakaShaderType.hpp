#ifndef MISAKASHADERTYPE_HPP
#define MISAKASHADERTYPE_HPP

#include <qopengl.h>
#include <Kawaii3D/Shaders/KawaiiShaderType.hpp>
#include "../Misaka3D_global.hpp"

enum class MisakaShaderType: GLenum
{
  Compute = GL_COMPUTE_SHADER,
  Vertex = GL_VERTEX_SHADER,
  TessCtrl = GL_TESS_CONTROL_SHADER,
  TessEvalution = GL_TESS_EVALUATION_SHADER,
  Geometry = GL_GEOMETRY_SHADER,
  Fragment = GL_FRAGMENT_SHADER,
  Invalid = 0
};

MISAKA3D_SHARED_EXPORT MisakaShaderType getMisakaShaderType(KawaiiShaderType type);

#endif // MISAKASHADERTYPE_HPP
