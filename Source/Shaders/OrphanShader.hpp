#ifndef ORPHANSHADER_HPP
#define ORPHANSHADER_HPP

#include "../OrphanGLData.hpp"
#include "../Misaka3D_global.hpp"
#include <qopengl.h>

class MISAKA3D_SHARED_EXPORT OrphanShader : public OrphanGLData
{
  GLuint id;
public:
  OrphanShader(GLuint id);
  ~OrphanShader() = default;

  // OrphanGLData interface
public:
  void destroy(MisakaFunctions &gl) override;
};

#endif // ORPHANSHADER_HPP
