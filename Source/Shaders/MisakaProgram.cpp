#include <Kawaii3D/KawaiiConfig.hpp>
#include "MisakaProgram.hpp"
#include "OrphanProgramm.hpp"
#include <QDebug>

MisakaProgram::MisakaProgram(MisakaContext3D *ctx):
  MisakaGLObject(ctx)
{
}

MisakaProgram::~MisakaProgram()
{
}

void MisakaProgram::attachShader(GLuint shader)
{
  if(!shaders.contains(shader))
    {
      shaders.insert(shader);
      attaching_shaders.push_back(shader);

      update();
    }
}

void MisakaProgram::detachShader(GLuint shader)
{
  auto el = shaders.find(shader);
  if(el != shaders.end())
    {
      detaching_shaders.push_back(shader);
      shaders.erase(el);

      update();
    }
}

void MisakaProgram::use() const
{
  gl().glUseProgram(ID());
}

void MisakaProgram::initialize(GLuint &id)
{
  id = gl().glCreateProgram();
  setOrphanHandle(new OrphanProgramm(id));
}

void MisakaProgram::updateNow()
{
  while(!attaching_shaders.isEmpty())
    gl().glAttachShader(ID(), attaching_shaders.takeFirst());

  while(!detaching_shaders.isEmpty())
    gl().glDetachShader(ID(), detaching_shaders.takeFirst());

  gl().glLinkProgram(ID());

  GLint success;
  gl().glGetProgramiv(ID(), GL_LINK_STATUS, &success);
  if(!success && KawaiiConfig::getInstance().getDebugLevel() > 0)
  {
      GLint logSize;
      gl().glGetProgramiv(ID(), GL_INFO_LOG_LENGTH, &logSize);
      std::vector<char> linkLog(logSize+1, '\0');
      gl().glGetProgramInfoLog(ID(), logSize, nullptr, &linkLog[0]);
      QString linkLogStr(&linkLog[0]);

      qCritical().noquote() << objectName() << " (MisakaProgram): Linking failed!\n" << linkLogStr;
      qCritical().quote();
  }
}
