#ifndef MISAKAGLBUFBINDING_HPP
#define MISAKAGLBUFBINDING_HPP

#include "MisakaGLBufferType.hpp"
#include "MisakaGLObject.hpp"

#include <Kawaii3D/KawaiiBufferTarget.hpp>

class MisakaGpuBufImpl;
class MISAKA3D_SHARED_EXPORT MisakaGLBufBinding: public MisakaGLObject
{
  Q_OBJECT
  friend class MisakaBufBindingImpl;

public:
  template<typename... ArgsT>
  MisakaGLBufBinding(GLuint bindingPoint,
                     MisakaGpuBufImpl *buffer,
                     ArgsT&&... args):
    MisakaGLObject(std::forward<ArgsT>(args)...),
    buffer(buffer),
    bindingPoint(bindingPoint)
  {
  }

  void setTarget(KawaiiBufferTarget target);
  MisakaGLBufferType getTarget() const;

  // MisakaGLObject interface
private:
  void initialize(GLuint &id) override final;
  inline void updateNow() override final {}



  //IMPLEMENT
private:
  MisakaGpuBufImpl *buffer;
  GLuint bindingPoint;
  MisakaGLBufferType target;
};

#endif // MISAKAGLBUFBINDING_HPP
