#include "MisakaGLBuffer.hpp"
#include "OrphanGLBuffer.hpp"

MisakaGLBuffer::~MisakaGLBuffer()
{
}

void MisakaGLBuffer::setData(const void *ptr, size_t size)
{
  dirtyIntervals.clear();
  data = ptr;
  if(dataSize != size) {
      dataSize = size;
      return reinit();
    }
  else
    dirtyIntervals.push_back({0, size});
  update();
}

bool MisakaGLBuffer::bind()
{
  if(isValid())
    {
      gl().glBindBuffer(type, ID());
      return true;
    } else
    return false;
}

bool MisakaGLBuffer::bindToBlock(GLuint blockIndex, MisakaGLBufferType target) const
{
  bool result = isValid();
  if(result) {
      switch(target) {
        case MisakaGLBufferType::UBO:
        case MisakaGLBufferType::SSBO:
        case MisakaGLBufferType::AtomicCounter:
        case MisakaGLBufferType::TransformFeedback:
          gl().glBindBufferBase(target, blockIndex, ID());
          break;

        default:
          result = false;
        }
    }
  return result;
}

bool MisakaGLBuffer::bindToBlock(GLuint blockIndex) const
{
  return bindToBlock(blockIndex, type);
}

bool MisakaGLBuffer::bindVertexBuffer(GLuint vaobj, GLuint bindingindex, GLintptr offset, GLsizei stride) const
{
  if(isValid() && type == MisakaGLBufferType::VBO)
    {
      gl().glVertexArrayVertexBuffer(vaobj, bindingindex, ID(), offset, stride);
      return true;
    } else
    return false;
}

bool MisakaGLBuffer::bindElementBuffer(GLuint vaobj) const
{
  if(isValid() && type == MisakaGLBufferType::ElementArray)
    {
      gl().glVertexArrayElementBuffer(vaobj, ID());
      return true;
    } else
    return false;
}

bool MisakaGLBuffer::unbindBlock(GLuint blockIndex, MisakaGLBufferType target) const
{
  bool result = isValid();
  if(result) {
      switch(target) {
        case MisakaGLBufferType::UBO:
        case MisakaGLBufferType::SSBO:
        case MisakaGLBufferType::AtomicCounter:
        case MisakaGLBufferType::TransformFeedback:
          gl().glBindBufferBase(target, blockIndex, 0);
          break;

        default:
          result = false;
        }
    }
  return result;
}

void MisakaGLBuffer::updateData(size_t offset, size_t n)
{
  updateData(reinterpret_cast<const void*>(reinterpret_cast<size_t>(data) + offset), n);
}

void MisakaGLBuffer::updateData(const void *subData, size_t n)
{
  if(!isValid()) return;

  size_t A1 = reinterpret_cast<size_t>(subData),
      B1 = A1 + n,
      A0 = reinterpret_cast<size_t>(data),
      B0 = A0 + dataSize;

  if(A1 < A0 ||
     A1 > B0 ||
     B1 > B0)
    return;

  Interval newDirty{A1 - A0, n};
  for(auto i = dirtyIntervals.begin(); i != dirtyIntervals.end(); ++i)
    if(mergeIntervals(newDirty, *i))
      return condenseDirtyIntervals(i);

  dirtyIntervals.append(newDirty);
  update();
}

void MisakaGLBuffer::initialize(GLuint &id)
{
  if(data && dataSize)
    {
      gl().glCreateBuffers(1, &id);
      gl().glNamedBufferStorage(id, dataSize, data, GL_DYNAMIC_STORAGE_BIT);
      setOrphanHandle(new OrphanGLBuffer(id));
    } else
    id = 0;

  justInitialized = true;
}

void MisakaGLBuffer::updateNow()
{
  if(!ID())
    {
      if(data && dataSize)
        reinit();
      return;
    }

  if(justInitialized)
    {
      justInitialized = false;
      dirtyIntervals.clear();
      return;
    }

  if(dirtyIntervals.isEmpty())
    gl().glNamedBufferSubData(ID(), 0, dataSize, data);
  else {
      while(!dirtyIntervals.isEmpty())
        {
          auto i = dirtyIntervals.takeFirst();
          gl().glNamedBufferSubData(ID(), i.i, i.n, reinterpret_cast<const void*>(reinterpret_cast<size_t>(data) + i.i));
        }
    }
}

size_t MisakaGLBuffer::getDataSize() const
{
  return dataSize;
}

void MisakaGLBuffer::condenseDirtyIntervals(const QLinkedList<Interval>::iterator &el)
{
  for(auto j = dirtyIntervals.begin(); j != dirtyIntervals.end();)
    if(j != el && mergeIntervals(*el, *j))
      j = dirtyIntervals.erase(j);
    else
      ++j;
}

bool MisakaGLBuffer::mergeIntervals(const MisakaGLBuffer::Interval &a, MisakaGLBuffer::Interval &b)
{
  size_t A0 = reinterpret_cast<size_t>(data),
      A1 = a.i + A0,
      B1 = A1 + a.n,

      A2 = b.i + A0,
      B2 = A2 + b.n;

  if(B1 <= B2 && B1 >= A2)
    {
      if(A1 < A2)
        b.i = A1 - A0;
      return true;
    }

  if(A1 <= B2 && A1 >= A2)
    {
      if(B1 > B2)
        b.n = B1 - A2;
      return true;
    }
  return false;
}
