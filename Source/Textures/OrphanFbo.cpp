#include "OrphanFbo.hpp"
#include "MisakaFunctions.hpp"

OrphanFbo::OrphanFbo(GLuint id):
  id(id)
{ }

void OrphanFbo::destroy(MisakaFunctions &gl)
{
  gl.glDeleteFramebuffers(1, &id);
}
