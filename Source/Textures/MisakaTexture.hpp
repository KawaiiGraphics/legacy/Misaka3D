#ifndef MISAKATEXTURE_HPP
#define MISAKATEXTURE_HPP

#include <QVector>
#include "../MisakaGLObject.hpp"
#include "MisakaTextureType.hpp"
#include "MisakaTextureFilter.hpp"
#include "MisakaTextureWrapMode.hpp"

class MISAKA3D_SHARED_EXPORT MisakaTexture : public MisakaGLObject
{
  Q_OBJECT
  friend class MisakaTextureImpl;
protected:
  MisakaTexture(MisakaContext3D *ctx, const glm::uvec4 &sz, MisakaTextureType type);
  void setSize(const glm::uvec4 &sz);

  void setMinFilter(MisakaTextureFilter value);
  void setMagFilter(MisakaTextureFilter value);

  void setWrapModeS(MisakaTextureWrapMode value);
  void setWrapModeT(MisakaTextureWrapMode value);
  void setWrapModeR(MisakaTextureWrapMode value);

protected:
  MisakaTextureFilter getMinFilter() const;
  MisakaTextureFilter getMagFilter() const;

public:
  ~MisakaTexture();

  inline auto& getSize() const
  { return currentSz; }

  inline MisakaTextureType getType() const
  { return type; }

  void attachToFbo(GLenum mode) const;
  void attachLayerToFbo(int layer, GLenum mode) const;

signals:
  void handleChanged(GLuint64 handle);

  // MisakaGLObject interface
protected:
  void initialize(GLuint &id) override;
  void updateNow() override;



  //IMPLEMENT
private:
  glm::uvec4 currentSz;
  MisakaTextureType type;
  GLuint64 handle;

  MisakaTextureFilter minFilter;
  MisakaTextureFilter magFilter;

  MisakaTextureWrapMode wrapModeS;
  MisakaTextureWrapMode wrapModeT;
  MisakaTextureWrapMode wrapModeR;

  inline virtual void fill() {}
};

#endif // MISAKATEXTURE_HPP
