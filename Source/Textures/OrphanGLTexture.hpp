#ifndef ORPHANGLTEXTURE_HPP
#define ORPHANGLTEXTURE_HPP

#include "../OrphanGLData.hpp"
#include "../Misaka3D_global.hpp"
#include <qopengl.h>

class MISAKA3D_SHARED_EXPORT OrphanGLTexture : public OrphanGLData
{
  GLuint id;
public:
  OrphanGLTexture(GLuint id);
  ~OrphanGLTexture() = default;

  // OrphanGLData interface
  void destroy(MisakaFunctions &gl) override;
};

#endif // ORPHANGLTEXTURE_HPP
