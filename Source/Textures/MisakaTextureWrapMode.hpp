#ifndef MISAKATEXTUREWRAPMODE_HPP
#define MISAKATEXTUREWRAPMODE_HPP

#include <Kawaii3D/Textures/KawaiiTextureWrapMode.hpp>
#include "Misaka3D_global.hpp"
#include <qopengl.h>

enum MisakaTextureWrapMode: GLenum
{
  ClampToEdge         = GL_CLAMP_TO_EDGE,
  ClampToBorder       = GL_CLAMP_TO_BORDER,
  MirroredRepeat      = GL_MIRRORED_REPEAT,
  Repeat              = GL_REPEAT,
  MirroredClampToEdge = GL_MIRROR_CLAMP_TO_EDGE
};

MisakaTextureWrapMode getMisakaTextureWrapMode(KawaiiTextureWrapMode value);

#endif // MISAKATEXTUREWRAPMODE_HPP
