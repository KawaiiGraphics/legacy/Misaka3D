#ifndef MISAKATEXBINDINGIMPL_HPP
#define MISAKATEXBINDINGIMPL_HPP

#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include <Kawaii3D/Textures/KawaiiTexBinding.hpp>
#include "Misaka3D_global.hpp"

class MisakaTextureImpl;
class MisakaGpuBufImpl;

class MISAKA3D_SHARED_EXPORT MisakaTexBindingImpl: public KawaiiRendererImpl
{
  Q_OBJECT
public:
  explicit MisakaTexBindingImpl(KawaiiTexBinding *model);
  ~MisakaTexBindingImpl();



  //IMPLEMENT
private:
  QMetaObject::Connection onTargetHandleChanged;
  KawaiiTexBinding *model;
  QString bindingName;

  void bind();
  void unbind();
  void onTargetChanged();
  void onBindingNameChanged(const QString &bindingName);

  MisakaTextureImpl *getTarget() const;
  MisakaGpuBufImpl *getOwner() const;

  // KawaiiRendererImpl interface
private:
  void initConnections() override;
};

#endif // MISAKATEXBINDINGIMPL_HPP
