#ifndef MISAKAFRAMEBUFFERIMPL_HPP
#define MISAKAFRAMEBUFFERIMPL_HPP

#include "../MisakaRootImpl.hpp"
#include "MisakaRenderbuffer.hpp"
#include "MisakaTextureImpl.hpp"
#include <Kawaii3D/Textures/KawaiiFramebuffer.hpp>

class MisakaCameraImpl;
class MisakaGpuBufImpl;

class MISAKA3D_SHARED_EXPORT MisakaFramebufferImpl: public KawaiiRendererImpl
{
public:
  explicit MisakaFramebufferImpl(KawaiiFramebuffer *model);
  ~MisakaFramebufferImpl();



  //IMPLEMENT
private:
  MisakaContext3D::OffscreenRenderIterator renderI;

  QPointer<MisakaRenderbuffer> depthRenderbuffer;
  KawaiiFramebuffer *model;
  MisakaRootImpl *root;
  MisakaFbo *fbo;

  void render() const;
  MisakaCameraImpl *getCamera() const;
  MisakaGpuBufImpl *getSfcBuf() const;

  void onDataParentChanged();
  void offscrRender();
};

#endif // MISAKAFRAMEBUFFER_HPP
