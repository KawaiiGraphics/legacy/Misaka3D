#include "MisakaImgImpl.hpp"
#include "MisakaRootImpl.hpp"
#include "MisakaTextureImpl.hpp"
#include <Kawaii3D/KawaiiRoot.hpp>

MisakaImgImpl::MisakaImgImpl(KawaiiImage *model):
  MisakaTextureImpl(model),
  model(model),
  root(nullptr)
{
  if(model)
    {
      connect(model, &KawaiiImage::imageUpdated, this, &MisakaImgImpl::updateImg);
      connect(model, &KawaiiImage::parentUpdated, this, &MisakaImgImpl::createView);

      createView();
    } else
    deleteLater();
}

MisakaImgImpl::~MisakaImgImpl()
{
}

void MisakaImgImpl::createView()
{
  auto _root = MisakaRootImpl::findRootImpl(this);
  if(root != _root)
    {
      root = _root;
      setView(root? new MisakaImg(root->getContext(), &model->getImage()): nullptr);
    }
}

void MisakaImgImpl::updateImg()
{
  if(getView())
    getView()->update();
}

