#ifndef MISAKATEXTURETYPE_HPP
#define MISAKATEXTURETYPE_HPP

#include <qopengl.h>
#include <glm/vec4.hpp>
#include <Kawaii3D/Textures/KawaiiTextureType.hpp>
#include "../Misaka3D_global.hpp"

class MisakaFunctions;

enum class MisakaTextureType: GLenum
{
  Texture1D = GL_TEXTURE_1D,
  Texture2D = GL_TEXTURE_2D,
  Texture3D = GL_TEXTURE_3D,
  Texture1D_Array = GL_TEXTURE_1D_ARRAY,
  Texture2D_Array = GL_TEXTURE_2D_ARRAY,
  TextureRectangle = GL_TEXTURE_RECTANGLE,
  TextureCube = GL_TEXTURE_CUBE_MAP,
  TextureCube_Array = GL_TEXTURE_CUBE_MAP_ARRAY,
  TextureBuffer = GL_TEXTURE_BUFFER,
  Texture2D_Multisample = GL_TEXTURE_2D_MULTISAMPLE,
  Texture2D_Multisample_Array = GL_TEXTURE_2D_MULTISAMPLE_ARRAY,
  Invalid = 0
};

MISAKA3D_SHARED_EXPORT MisakaTextureType getMisakaTextureType(KawaiiTextureType type);

#endif // MISAKATEXTURETYPE_HPP
