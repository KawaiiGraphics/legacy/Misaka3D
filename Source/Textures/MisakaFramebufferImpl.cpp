#include "MisakaFramebufferImpl.hpp"
#include <Kawaii3D/KawaiiRoot.hpp>
#include "MisakaTextureImpl.hpp"
#include "MisakaCameraImpl.hpp"
#include "MisakaRootImpl.hpp"
#include "MisakaFbo.hpp"

MisakaFramebufferImpl::MisakaFramebufferImpl(KawaiiFramebuffer *model):
  KawaiiRendererImpl(model),
  model(model),
  root(nullptr),
  fbo(nullptr)
{
  if(model)
    {
      connect(model, &KawaiiFramebuffer::parentUpdated, this, &MisakaFramebufferImpl::onDataParentChanged);
      onDataParentChanged();
    } else
    deleteLater();
}

MisakaFramebufferImpl::~MisakaFramebufferImpl()
{
  if(depthRenderbuffer)
    {
      auto _depthRenderbuffer = depthRenderbuffer.data();
      depthRenderbuffer.clear();
      delete _depthRenderbuffer;
    }

  root->getContext()->removeOffscreenRender(renderI);
}

void MisakaFramebufferImpl::render() const
{
  auto cam = getCamera();
  auto sfcBuf = getSfcBuf();
  if(cam && sfcBuf)
    cam->draw(sfcBuf->getBuffer());
}

MisakaCameraImpl *MisakaFramebufferImpl::getCamera() const
{
  if(model->getCamera())
    return static_cast<MisakaCameraImpl*>(model->getCamera()->getRendererImpl());
  else
    return nullptr;
}

MisakaGpuBufImpl *MisakaFramebufferImpl::getSfcBuf() const
{
  if(model->getSfcUbo())
    return static_cast<MisakaGpuBufImpl*>(model->getSfcUbo()->getRendererImpl());
  else
    return nullptr;
}

void MisakaFramebufferImpl::onDataParentChanged()
{
  if(root)
    root->getContext()->removeOffscreenRender(renderI);

  root = MisakaRootImpl::findRootImpl(this);
  if(root)
    {
      fbo = root->getContext()->getOffscreenRender();
      renderI = root->getContext()->addOffscreenRender(std::bind(&MisakaFramebufferImpl::offscrRender, this));
    } else
    fbo = nullptr;
}

void MisakaFramebufferImpl::offscrRender()
{
  bool hasDepth = false,
      hasAtt = false;
  auto *_fbo = fbo;
  model->forallAtachments([_fbo, &hasDepth, &hasAtt] (const KawaiiFramebuffer::Attachment &att) {
      if(att.target->getRendererImpl())
        {
          hasAtt = true;
          hasDepth |= (att.mode == KawaiiFramebuffer::AttachmentMode::Depth || att.mode == KawaiiFramebuffer::AttachmentMode::DepthStencil);
          static_cast<MisakaTextureImpl*>(att.target->getRendererImpl())->attachToFbo(_fbo, att.mode, att.layer);
        }
    });

  if(!hasDepth)
    {
      if(!depthRenderbuffer)
        depthRenderbuffer = new MisakaRenderbuffer(QSize(model->getSize().x, model->getSize().y), root->getContext());
      else
        depthRenderbuffer->setSize(QSize(model->getSize().x, model->getSize().y));

      fbo->attach(depthRenderbuffer.data(), GL_DEPTH_ATTACHMENT);
    }

  fbo->prepareRendering();
  render();
}
