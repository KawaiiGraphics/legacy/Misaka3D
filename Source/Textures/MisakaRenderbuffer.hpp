#ifndef MISAKARENDERBUFFER_HPP
#define MISAKARENDERBUFFER_HPP

#include "MisakaFbo.hpp"

class MISAKA3D_SHARED_EXPORT MisakaRenderbuffer: public MisakaGLObject
{
public:
  template<typename... ArgsT>
  MisakaRenderbuffer(const QSize &sz, ArgsT&&... args):
    MisakaGLObject(std::forward<ArgsT>(args)...),
    sz(sz)
  { }

  ~MisakaRenderbuffer();

  const QSize& getSize() const;
  void setSize(const QSize &size);

  void attachToFbo(GLenum mode) const;

  // MisakaGLObject interface
private:
  void initialize(GLuint &id) override;

  inline void updateNow() override
  {}



  //IMPLEMENT
  QSize sz;
};

#endif // MISAKARENDERBUFFER_HPP
