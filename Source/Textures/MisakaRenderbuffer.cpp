#include "MisakaRenderbuffer.hpp"
#include "OrphanRenderbuffer.hpp"

MisakaRenderbuffer::~MisakaRenderbuffer()
{
}

const QSize &MisakaRenderbuffer::getSize() const
{
  return sz;
}

void MisakaRenderbuffer::setSize(const QSize &size)
{
  if(sz == size)
    return;

  sz = size;
  reinit();
}

void MisakaRenderbuffer::attachToFbo(GLenum mode) const
{
  gl().glFramebufferRenderbuffer(GL_FRAMEBUFFER, mode, GL_RENDERBUFFER, ID());
}

void MisakaRenderbuffer::initialize(GLuint &id)
{
  gl().glCreateRenderbuffers(1, &id);
  gl().glNamedRenderbufferStorage(id, GL_DEPTH_COMPONENT, sz.width(), sz.height());
  setOrphanHandle(new OrphanRenderbuffer(id));
}
