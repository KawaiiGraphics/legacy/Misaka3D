#include "MisakaFbo.hpp"
#include "MisakaTexture.hpp"
#include "MisakaTextureImpl.hpp"
#include "../MisakaRootImpl.hpp"

#include <Kawaii3D/KawaiiRoot.hpp>
#include <Kawaii3D/Textures/KawaiiTexture.hpp>
#include <Kawaii3D/Exceptions/KawaiiException.hpp>

MisakaTextureImpl::MisakaTextureImpl(KawaiiTexture *model):
  KawaiiRendererImpl(model),
  model(model),
  view(nullptr)
{
  if(model)
    {
      connect(model, &KawaiiTexture::minFilterChanged, this, &MisakaTextureImpl::onModelMinFilterChanged);
      connect(model, &KawaiiTexture::magFilterChanged, this, &MisakaTextureImpl::onModelMagFilterChanged);

      connect(model, &KawaiiTexture::wrapModeSChanged, this, &MisakaTextureImpl::onModelWrapModeSChanged);
      connect(model, &KawaiiTexture::wrapModeTChanged, this, &MisakaTextureImpl::onModelWrapModeTChanged);
      connect(model, &KawaiiTexture::wrapModeRChanged, this, &MisakaTextureImpl::onModelWrapModeRChanged);
    }
}

MisakaTextureImpl::~MisakaTextureImpl()
{
  if(view)
    view->deleteLater();
}

uint64_t MisakaTextureImpl::getTextureHandle() const
{
  if(view && view->isValid())
    return view->handle;
  else
    return 0ull;
}

namespace {
  GLenum trAttMode(KawaiiFramebuffer::AttachmentMode mode)
  {
    switch(mode)
      {
      case KawaiiFramebuffer::AttachmentMode::Depth:        return GL_DEPTH_ATTACHMENT;
      case KawaiiFramebuffer::AttachmentMode::Stencil:      return GL_STENCIL_ATTACHMENT;
      case KawaiiFramebuffer::AttachmentMode::DepthStencil: return GL_DEPTH_STENCIL_ATTACHMENT;
      case KawaiiFramebuffer::AttachmentMode::Color0:       return GL_COLOR_ATTACHMENT0;
      case KawaiiFramebuffer::AttachmentMode::Color1:       return GL_COLOR_ATTACHMENT1;
      case KawaiiFramebuffer::AttachmentMode::Color2:       return GL_COLOR_ATTACHMENT2;
      case KawaiiFramebuffer::AttachmentMode::Color3:       return GL_COLOR_ATTACHMENT3;
      case KawaiiFramebuffer::AttachmentMode::Color4:       return GL_COLOR_ATTACHMENT4;
      case KawaiiFramebuffer::AttachmentMode::Color5:       return GL_COLOR_ATTACHMENT5;
      case KawaiiFramebuffer::AttachmentMode::Color6:       return GL_COLOR_ATTACHMENT6;
      case KawaiiFramebuffer::AttachmentMode::Color7:       return GL_COLOR_ATTACHMENT7;
      }
    return GL_COLOR_ATTACHMENT0;
  }
}

void MisakaTextureImpl::attachToFbo(MisakaFbo *fbo, KawaiiFramebuffer::AttachmentMode mode, int texLayer) const
{
  fbo->attach(getView(), texLayer, trAttMode(mode));
}

MisakaTexture *MisakaTextureImpl::getView() const
{
  return view.data();
}

void MisakaTextureImpl::setView(MisakaTexture *view)
{
  if(view == this->view)
    return;

  if(this->view)
    {
      disconnect(this->view, &MisakaTexture::handleChanged, this, &MisakaTextureImpl::textureHandleChanged);
      this->view->deleteLater();
    }

  this->view = view;

  if(this->view)
    {
      connect(this->view, &MisakaTexture::handleChanged, this, &MisakaTextureImpl::textureHandleChanged);
      this->view->setMinFilter(getMisakaTextureFilter(model->getMinFilter()));
      this->view->setMagFilter(getMisakaTextureFilter(model->getMagFilter()));

      this->view->setWrapModeS(getMisakaTextureWrapMode(model->getWrapModeS()));
      this->view->setWrapModeT(getMisakaTextureWrapMode(model->getWrapModeT()));
      this->view->setWrapModeR(getMisakaTextureWrapMode(model->getWrapModeR()));
    }
}

void MisakaTextureImpl::onModelMinFilterChanged(KawaiiTextureFilter filter)
{
  if(view)
    view->setMinFilter(getMisakaTextureFilter(filter));
}

void MisakaTextureImpl::onModelMagFilterChanged(KawaiiTextureFilter filter)
{
  if(view)
    view->setMagFilter(getMisakaTextureFilter(filter));
}

void MisakaTextureImpl::onModelWrapModeSChanged(KawaiiTextureWrapMode mode)
{
  if(view)
    view->setWrapModeS(getMisakaTextureWrapMode(mode));
}

void MisakaTextureImpl::onModelWrapModeTChanged(KawaiiTextureWrapMode mode)
{
  if(view)
    view->setWrapModeT(getMisakaTextureWrapMode(mode));
}

void MisakaTextureImpl::onModelWrapModeRChanged(KawaiiTextureWrapMode mode)
{
  if(view)
    view->setWrapModeR(getMisakaTextureWrapMode(mode));
}
