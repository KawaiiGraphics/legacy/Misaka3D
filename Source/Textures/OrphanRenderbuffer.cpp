#include "OrphanRenderbuffer.hpp"
#include "MisakaFunctions.hpp"

OrphanRenderbuffer::OrphanRenderbuffer(GLuint id):
  id(id)
{ }

void OrphanRenderbuffer::destroy(MisakaFunctions &gl)
{
  gl.glDeleteRenderbuffers(1, &id);
}
