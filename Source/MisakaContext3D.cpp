#include "MisakaContext3D.hpp"
#include "Textures/MisakaFbo.hpp"

#include <QRect>
#include <QDebug>
#include <QString>
#include <string_view>
#include <QSurfaceFormat>
#include <QOffscreenSurface>
#include <QProcessEnvironment>
#include <Kawaii3D/KawaiiConfig.hpp>

namespace {
  template<size_t N>
  constexpr QLatin1String latin1Str(const char (&str)[N])
  {
    return str[N-1] == '\0'? QLatin1String(str, N-1): QLatin1String(str, N);
  }

  void receiveGLDebug(GLenum source, GLenum type, GLuint id,
                      GLenum severity, GLsizei length, const GLchar* message, const void*)
  {
    const uint16_t dbg = KawaiiConfig::getInstance().getDebugLevel();

    static const std::unordered_map<GLenum, QLatin1String> sourceNames = {
      {GL_DEBUG_SOURCE_API,             latin1Str("calls to the OpenGL API")},
      {GL_DEBUG_SOURCE_WINDOW_SYSTEM,   latin1Str("calls to a window-system API")},
      {GL_DEBUG_SOURCE_SHADER_COMPILER, latin1Str("compiler for a shading language")},
      {GL_DEBUG_SOURCE_THIRD_PARTY,     latin1Str("application associated with OpenGL")},
      {GL_DEBUG_SOURCE_APPLICATION,     latin1Str("user of this application")},
      {GL_DEBUG_SOURCE_OTHER,           latin1Str("Unknown")}
    };

    static const std::unordered_map<GLenum, QLatin1String> typeNames = {
      {GL_DEBUG_TYPE_ERROR,               latin1Str("Error")},
      {GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR, latin1Str("Warning (deprecated)")},
      {GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR,  latin1Str("Warning (undefined behavior)")},
      {GL_DEBUG_TYPE_PORTABILITY,         latin1Str("Warning (functionality is not portable)")},
      {GL_DEBUG_TYPE_PERFORMANCE,         latin1Str("Warning (performance issues)")},
      {GL_DEBUG_TYPE_MARKER,              latin1Str("Command stream annotation")},
      {GL_DEBUG_TYPE_PUSH_GROUP,          latin1Str("Group pushed")},
      {GL_DEBUG_TYPE_POP_GROUP,           latin1Str("Group poped")},
      {GL_DEBUG_TYPE_OTHER,               latin1Str("Unknown")}
    };

    if(length < 0 || strlen(reinterpret_cast<const char*>(message)) < static_cast<size_t>(length))
      return;

    switch(severity)
      {
      case GL_DEBUG_SEVERITY_HIGH:
        qCritical().setAutoInsertSpaces(true);
        qCritical() << "OpenGL debug message #" << id << '{';
        qCritical() << "\tFrom " << sourceNames.at(source);
        qCritical() << "\tType: " << typeNames.at(type);
        qCritical() << "\tText: " << reinterpret_cast<const char*>(message);
        qCritical().nospace() << '}';
        break;

      case GL_DEBUG_SEVERITY_MEDIUM:
        if(dbg < 2) break;
        qWarning() << "OpenGL debug message #" << id << '{';
        qWarning() << "\tFrom " << sourceNames.at(source);
        qWarning() << "\tType: " << typeNames.at(type);
        qWarning() << "\tText: " << reinterpret_cast<const char*>(message);
        qWarning().nospace() << '}';
        break;

      default:
        if(dbg < 3) break;
        qInfo() << "OpenGL debug message #" << id << '{';
        qInfo() << "\tFrom " << sourceNames.at(source);
        qInfo() << "\tType: " << typeNames.at(type);
        qInfo() << "\tText: " << reinterpret_cast<const char*>(message);
        qInfo().nospace() << '}';
        break;
      }
  }
}

QSurfaceFormat& misakaSfcFormat(QSurfaceFormat &fmt)
{
  static QProcessEnvironment sysEnv = QProcessEnvironment::systemEnvironment();
  QStringList version = sysEnv.value("SIB3D_OVERRIDE_GL_VERSION", "4.5").split('.');

  fmt = KawaiiConfig::getInstance().getPreferredSfcFormat();
  fmt.setVersion(version.first().toInt(), version.at(1).toInt());
  fmt.setRenderableType(QSurfaceFormat::OpenGL);
  fmt.setProfile(QSurfaceFormat::CoreProfile);
  fmt.setOption(QSurfaceFormat::DeprecatedFunctions, false);

  return fmt;
}

inline QSurfaceFormat& misakaSfcFormat(QSurfaceFormat &&fmt)
{ return misakaSfcFormat(static_cast<QSurfaceFormat&>(fmt)); }

MisakaContext3D::MisakaContext3D(QObject *parent) :
  QObject(parent),
  defaultFbo(0)
{
  auto fmt = requestedFormat();

  QOffscreenSurface sfc;
  sfc.setFormat(fmt);
  sfc.create();

  d.setFormat(fmt);
  d.create();
  d.makeCurrent(&sfc);
  gl.initialize();
  gl.glEnable(GL_DEPTH_TEST);
  if(KawaiiConfig::getInstance().getDebugLevel() > 0)
    {
      gl.glEnable(GL_DEBUG_OUTPUT);
      gl.glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
      gl.glDebugMessageCallback(&receiveGLDebug, nullptr);
    }
  else
    gl.glDisable(GL_DEBUG_OUTPUT);

  gl.glClearColor(0,0,0, 1.0);

  defaultFbo = d.defaultFramebufferObject();

  d.doneCurrent();
  sfc.destroy();
}

MisakaContext3D::~MisakaContext3D()
{
  static QOffscreenSurface sfc;

  sfc.create();
  d.makeCurrent(&sfc);

  while(!orphan.isEmpty())
    OrphanGLData::destroyData(orphan.takeFirst(), gl);

  for(auto *ptr: glObjects)
    {
      ptr->destroy();
      delete ptr;
    }
  sfc.destroy();
}

QSurfaceFormat &MisakaContext3D::requestedFormat()
{
  static auto fmt = misakaSfcFormat(QSurfaceFormat::defaultFormat());
  return fmt;
}

QSurfaceFormat &MisakaContext3D::requestedFormat(QSurfaceFormat &&original)
{
  return misakaSfcFormat(original);
}

void MisakaContext3D::beginDraw(QSurface *sfc)
{
  d.makeCurrent(sfc);

  for(auto *ptr: glObjects)
    ptr->checkDirtyChildren();

  dirtyObjects.check();

  while(!orphan.isEmpty())
    OrphanGLData::destroyData(orphan.takeFirst(), gl);

  if(!offscrRenderHooks.empty())
    {
      getOffscreenRender()->bind();
      for(const auto &i: offscrRenderHooks)
        i();
      getOffscreenRender()->unbind(defaultFbo);
      gl.glEnable(GL_DEPTH_TEST);
    }

  gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void MisakaContext3D::endDraw(QSurface *sfc)
{
  d.swapBuffers(sfc);
  d.doneCurrent();
}

MisakaContext3D::OffscreenRenderIterator MisakaContext3D::addOffscreenRender(std::function<void ()> &&hook)
{
  int count = offscrRenderHooks.size();
  offscrRenderHooks.push_back(hook);
  return OffscreenRenderIterator(this, count);
}

MisakaContext3D::OffscreenRenderIterator MisakaContext3D::addOffscreenRender(std::function<void ()> &hook)
{
  int count = offscrRenderHooks.size();
  offscrRenderHooks.push_back(hook);
  return OffscreenRenderIterator(this, count);
}

void MisakaContext3D::removeOffscreenRender(const OffscreenRenderIterator &pos)
{
  if(pos.ctx != this)
    return;


  offscrRenderHooks.erase(offscrRenderHooks.begin() + pos.index);
  for(auto *iter: offscrIterators)
    {
      if(iter->index == pos.index)
        {
          iter->index = -1;
          iter->ctx = nullptr;
        }

      if(iter->index > pos.index)
        iter->index--;
    }
}

MisakaFbo *MisakaContext3D::getOffscreenRender()
{
  if(!offscreenFbo)
    {
      offscreenFbo = new MisakaFbo(this);
      if(QOpenGLContext::currentContext() == &d)
        dirtyObjects.check();
    }

  return offscreenFbo.data();
}

bool MisakaContext3D::checkSystem()
{
  const bool dbg_enabled = KawaiiConfig::getInstance().getDebugLevel() > 0;
  auto dbg_prnt = [&dbg_enabled] (const auto &str) {
      if(dbg_enabled)
        qDebug(str);
    };

  auto fmt = requestedFormat();

  QOffscreenSurface sfc;
  sfc.setFormat(fmt);
  sfc.create();

  QOpenGLContext ctx;
  ctx.setFormat(fmt);

  if(!ctx.create())
    {
      dbg_prnt("Misaka3D failed to init OpenGL 4.5 Core context!");
      return false;
    }

  if(!ctx.makeCurrent(&sfc))
    {
      dbg_prnt("Misaka3D failed to make OpenGL 4.5 Core context current!");
      return false;
    }

  MisakaFunctions gl_func;
  try
  {
    gl_func.initialize();
  } catch(...)
  {
    ctx.doneCurrent();
    sfc.destroy();
    dbg_prnt("Misaka3D failed to init OpenGL functions!");
    return false;
  }

  ctx.doneCurrent();
  sfc.destroy();

  auto issues = gl_func.check();
  bool result = true;
  if(issues.No_BindlessTexture)
    {
      dbg_prnt("Misaka3D failed to access bindless texture");
      result = false;
    }
  if(issues.No_DSA_Textures)
    {
      dbg_prnt("Misaka3D failed to access DSA textures");
      result = false;
    }
  if(issues.No_DSA_Buffers)
    {
      dbg_prnt("Misaka3D failed to access DSA buffers");
      result = false;
    }
  if(issues.No_DSA_VAO)
    {
      dbg_prnt("Misaka3D failed to access DSA VAO");
      result = false;
    }
  if(issues.No_DSA_FBO)
    {
      dbg_prnt("Misaka3D failed to access DSA FBO");
      result = false;
    }
  if(issues.No_DSA_RBO)
    {
      dbg_prnt("Misaka3D failed to access DSA RBO");
      result = false;
    }

  return result;
}

void MisakaContext3D::deleteOffscreenRender()
{
  offscreenFbo->deleteLater();
}

void MisakaContext3D::setViewport(const QRect &viewport)
{
  gl.glViewport(viewport);
}




MisakaContext3D::OffscreenRenderIterator::OffscreenRenderIterator(MisakaContext3D *ctx, int index):
  index(ctx? index: -1),
  ctx(ctx)
{
  if(ctx && index>=0 && static_cast<size_t>(index) < ctx->offscrRenderHooks.size())
    ctx->offscrIterators.push_back(this);
  else
    {
      this->ctx = nullptr;
      this->index = -1;
    }
}

MisakaContext3D::OffscreenRenderIterator::OffscreenRenderIterator():
  OffscreenRenderIterator(nullptr, -1)
{ }

MisakaContext3D::OffscreenRenderIterator::~OffscreenRenderIterator()
{
  if(ctx)
    ctx->offscrIterators.removeOne(this);
}

std::function<void()> &MisakaContext3D::OffscreenRenderIterator::value() const
{
  Q_ASSERT_X(ctx, "Memory access", "Invalid iterator: ctx is nullptr");

  return ctx->offscrRenderHooks[index];
}
